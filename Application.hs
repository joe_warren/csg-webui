{-# LANGUAGE OverloadedStrings #-}
module Application where

import Control.Monad
import Control.Monad.IO.Class
import CsgWeb.Common
import Web.Simple
import Web.Simple.Static
import Data.List
import qualified Data.ByteString.Char8 as S8
import qualified Data.ByteString.Lazy as BSL
import qualified Data.ByteString.Lazy.Char8 as BSL8
import qualified Data.Text.Encoding as Encoding
import qualified Csg
import qualified Csg.STL as STL
import qualified Data.Vec3 as V3
import Network.HTTP.Types.Method
import Network.HTTP.Types.Status
import Network.HTTP.Types.Header
import System.IO.Temp
import System.IO
import System.Directory
import qualified Language.Haskell.Interpreter as HS

runScriptHS :: FilePath -> HS.Interpreter (Csg.BspTree)
runScriptHS tmpDir = do
    HS.set [HS.searchPath HS.:= [tmpDir, "."]]
    sp <- HS.get HS.searchPath
    HS.liftIO $ do
        files <- mapM getDirectoryContents sp
        putStrLn $ show files
    HS.loadModules [tmpDir ++ "/Object.hs"]
    HS.setTopLevelModules ["Object"]
    HS.setImportsQ [("Csg", Nothing)]
    object <- HS.interpret "object" (HS.as::Csg.BspTree)
    return (object)

errorString :: HS.InterpreterError -> String
errorString (HS.WontCompile es) = intercalate "\n" (header : map unbox es)
  where
    header = "ERROR: Won't compile:"
    unbox (HS.GhcError e) = e
errorString e = show e

runScript :: S8.ByteString -> FilePath -> IO (Either String Csg.BspTree)
runScript code tmpDir = do
    S8.writeFile (tmpDir ++ "/Object.hs") code
    r <- HS.runInterpreter $ runScriptHS tmpDir
    case r of
        Left err -> return (Left $ errorString err)
        Right tree -> return $ Right tree


mapTuple3 :: (a -> b) -> (a, a, a) -> (b, b, b)
mapTuple3 f (a1, a2, a3) = (f a1, f a2, f a3)

tupleList :: (a, a, a) -> [a]
tupleList (x, y, z) = [x, y, z]

respondError :: String -> Response
respondError err = responseLBS badRequest400 [(hContentType, S8.pack "text/plain")] (BSL8.pack err)

toJson :: Csg.BspTree -> BSL.ByteString
toJson tree = BSL8.pack $ show $ concat $ concat $ triList
    where
        tris = Csg.toTris tree
        triDoubles = map (mapTuple3 V3.toXYZ) tris
        triList = map (tupleList.(mapTuple3 tupleList)) triDoubles
        

build :: Controller AppSettings ()
build = do
    (params, _) <- parseForm
    let notNull = not . S8.null
    let code = do
        text <- notNull `mfilter` lookup "code" params
        return text
    case code of
        Nothing -> redirectBack
        Just text -> do
            tree <- liftIO (withSystemTempDirectory "dir.tmp" (runScript text))
            case tree of
                Left err -> respond $ respondError err
                Right tree -> respond $ okJson $ toJson tree


export :: Controller AppSettings ()
export = do
    (params, _) <- parseForm
    let notNull = not . S8.null
    let code = do
        text <- notNull `mfilter` lookup "code" params
        return text
    case code of
        Nothing -> redirectBack
        Just text -> do
            tree <- liftIO (withSystemTempDirectory "dir.tmp" (runScript text))
            case tree of
                Left err -> respond $ respondError err
                Right tree -> respond $ ok "application/sla" (BSL8.fromStrict $ Encoding.encodeUtf8 $ STL.toSTL tree)


app :: (Application -> IO ()) -> IO ()
app runner = do
  settings <- newAppSettings

  runner $ controllerApp settings $ do
    routeTop $ serveStatic "static"
    routeName "static" $ serveStatic "static"
    routeName "build" $ do
        routeMethod POST build
    routeName "export.stl" $ do
        routeMethod POST export

