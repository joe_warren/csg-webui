{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
import qualified Data.Text.IO as T
import qualified Csg 
import qualified Csg.STL as STL
import Control.Monad
import Options.Applicative
import Data.Semigroup
import Data.List
import Data.Vec3 as V3
import qualified Language.Haskell.Interpreter as HS

data Opts = Opts String String 

opts :: Parser Opts
opts = Opts 
    <$> strArgument 
        (metavar "FILE" 
        <> help "Input script file")
    <*> strArgument 
        (metavar "Output" 
        <> help "Output STL")



say :: String -> HS.Interpreter ()
say = HS.liftIO . putStrLn

runScript :: String -> HS.Interpreter (Csg.BspTree)
runScript script = do
    HS.loadModules [script]
    HS.setTopLevelModules [(takeWhile (/='.') script)]
    HS.setImportsQ [("Csg", Nothing)]
    object <- HS.interpret "object" (HS.as::Csg.BspTree)
    return (object)

errorString :: HS.InterpreterError -> String
errorString (HS.WontCompile es) = intercalate "\n" (header : map unbox es)
  where
    header = "ERROR: Won't compile:"
    unbox (HS.GhcError e) = e
errorString e = show e

runScriptIO :: Opts -> IO ()
runScriptIO (Opts script output) = do
    r <- HS.runInterpreter (runScript script)
    case r of
        Left err -> putStrLn $ errorString err
        Right tree -> T.writeFile output $ STL.toSTL tree

main :: IO ()
main = execParser withHelp >>= runScriptIO where
  withHelp = info (helper <*> opts)
               ( fullDesc <> progDesc "read a script and produce an STL"
                 <> header "read a script and produce an STL" )
