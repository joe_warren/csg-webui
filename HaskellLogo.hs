{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Object where

import qualified Csg
import Data.List

object :: Csg.BspTree
object = bb `Csg.intersection` (back_chevron `Csg.union` lambda `Csg.union` equals_shifted)
    where 
        height = 6
        bar_height = height *2
        bar = Csg.rotate (0.0, 0.0, 1.0) (pi/6.0) $ Csg.translate (-0.50, bar_height/2.0, 0.0) $ Csg.scale (1.0, bar_height, 1.0) $ Csg.unitCube
        mirrored = Csg.scale (1.0, -1.0, -1.0) bar
        chevron = Csg.union bar mirrored
        back_chevron = Csg.translate (-2.0, 0.0, 0.0) chevron
        under = Csg.rotate (0.0, 0.0, 1.0) (pi+pi/6.0+0.0) $ Csg.translate (0.5001, bar_height/2.0, 0.0) $ Csg.scale (1.0, bar_height, 1.0) $ Csg.unitCube
        bb = Csg.scale (10, height, 2.0) Csg.unitCube
        equalsBar1 = Csg.translate (0.500, 1.25, 0.0) $ Csg.scale (3.0, 1.0, 0.999) $ Csg.unitCube
        equalsBar2 = Csg.translate (2/2 + 0.0001, -1.25, 0.0) $ Csg.scale (2, 1.0, 0.999) $ Csg.unitCube
        equals = equalsBar1 `Csg.union` equalsBar2
        equals_erased = equals `Csg.subtract` lambda
        equals_shifted = Csg.translate (1.0, 0.0, 0.0) equals_erased
        lambda = Csg.union under chevron
